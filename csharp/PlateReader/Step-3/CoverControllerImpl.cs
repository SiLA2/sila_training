using System;
using System.Threading.Tasks;
using Grpc.Core;
using Sila2.Org.Silastandard.Training.Covercontroller.V1;
using Sila2.Utils;

namespace SilaTraining.PlateReader
{
    public class CoverControllerImpl : CoverController.CoverControllerBase
    {
        public override Task<OpenCover_Responses> OpenCover(OpenCover_Parameters request, ServerCallContext context)
        {
            ErrorHandling.RaiseSiLAError(
                ErrorHandling.CreateDefinedExecutionError("CoverOperationInterrupted",
                    "Something is blocking the door"));
            // Unreachable since an exception is raised on the line above
            return Task.FromResult(new OpenCover_Responses());
        }
    }
}