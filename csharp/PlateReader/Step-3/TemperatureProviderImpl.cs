using System;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Training.Temperatureprovider.V1;

namespace SilaTraining.PlateReader
{
    public class TemperatureProviderImpl : TemperatureProvider.TemperatureProviderBase
    {
        public override Task<Get_CurrentTemperature_Responses> Get_CurrentTemperature(
            Get_CurrentTemperature_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_CurrentTemperature_Responses
            {
                CurrentTemperature = new Real
                {
                    Value = ReadTemperatureFromProbe()
                }
            });
        }

        private static double ReadTemperatureFromProbe()
        {
            return new Random().Next(-40, 80) / 2.0; // Random number between -20 and 40
        }
    }
}