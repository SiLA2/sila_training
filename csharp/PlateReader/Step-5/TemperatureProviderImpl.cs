using System;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Training.Temperatureprovider.V1;

namespace SilaTraining.PlateReader
{
    public class TemperatureProviderImpl : TemperatureProvider.TemperatureProviderBase
    {
        public override Task Subscribe_CurrentTemperature(Subscribe_CurrentTemperature_Parameters request, IServerStreamWriter<Subscribe_CurrentTemperature_Responses> responseStream,
            ServerCallContext context)
        {
            while (!context.CancellationToken.IsCancellationRequested)
            {
                responseStream.WriteAsync(new Subscribe_CurrentTemperature_Responses
                {
                    CurrentTemperature = new Real
                    {
                        Value = ReadTemperatureFromProbe()
                    }
                });
                Thread.Sleep(2000);
            }
            return Task.CompletedTask;
        }

        private static double ReadTemperatureFromProbe()
        {
            return new Random().Next(-40, 80) / 2.0; // Random number between -20 and 40
        }
    }
}