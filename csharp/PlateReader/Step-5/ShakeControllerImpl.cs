using System;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Sila2;
using Sila2.Org.Silastandard;
using Sila2.Org.Silastandard.Training.Shakecontroller.V1;

namespace SilaTraining.PlateReader
{
    public class ShakeControllerImpl : ShakeController.ShakeControllerBase
    {
        private readonly ObservableCommandManager<Shake_Parameters, Shake_Responses> _shakeManager =
            new ObservableCommandManager<Shake_Parameters, Shake_Responses>(TimeSpan.FromDays(1));

        public override async Task<CommandConfirmation> Shake(Shake_Parameters request, ServerCallContext context)
        {
            var command = await _shakeManager.AddCommand(request, ShakeTask);
            return command.Confirmation;
        }

        private static Shake_Responses ShakeTask(IProgress<ExecutionInfo> progress, Shake_Parameters request,
            CancellationToken token)
        {
            var secondsToShake = (int) request.Duration.Value;
            // Shake for the specified amount of time or cancel if cancellation requested
            token.WaitHandle.WaitOne(TimeSpan.FromSeconds(secondsToShake));
            return new Shake_Responses();
        }

        public override async Task Shake_Info(CommandExecutionUUID request,
            IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            await _shakeManager.RegisterForInfo(request, responseStream, context.CancellationToken);
        }

        public override Task<Shake_Responses> Shake_Result(CommandExecutionUUID request, ServerCallContext context)
        {
            var command = _shakeManager.GetCommand(request);
            return Task.FromResult(command.Result());
        }
    }
}