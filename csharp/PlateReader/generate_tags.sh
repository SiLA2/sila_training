#!/usr/bin/env bash

function move_and_commit() {
    rm -rf Final
    step_dir=${1##*/}
    cp -r ${step_dir} Final
    git add Final
    git commit -m "Add $step_dir to Final"
    git tag -a csharp.plate_reader.${step_dir} -m '$step_dir of Plate Reader'
    echo "csharp.plate_reader.$step_dir"
}

for d in ./*;
do
    if [[ -d $d ]]; then
        move_and_commit "$d";  
    fi
done