﻿using System;
using System.Net.NetworkInformation;
using NLog;
using Sila2.Utils;

namespace SilaTraining.PlateReader
{
    class Program
    {
        static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var log = LogManager.GetCurrentClassLogger();

            var options = CmdLineArgs.Parse<ServerCmdLineArgs>(args);
            Server server;
            NetworkInterface networkInterface;
            try
            {
                // create SiLA2 Server
                networkInterface = Networking.GetNetworkInterface(options.InterfaceNameOrCIDR);
            }
            catch (ArgumentNullException)
            {
                // create SiLA2 Server without discovery
                networkInterface = null;
            }

            server = new Server(options.Port, networkInterface, options.ConfigFile);

            log.Info("Example Server listening on port " + options.Port);

            log.Info("Press enter to stop the server...");
            Console.ReadLine();
        }
    }
}