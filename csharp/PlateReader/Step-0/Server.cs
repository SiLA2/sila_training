using System;
using Common.Logging;
using Sila2.Server;

namespace SilaTraining.PlateReader
{
    using System.Net.NetworkInformation;
    using Sila2;

    /// <summary>
    /// Implements a SiLA 2 Server that provides a GreetingProvider and a TemperatureController feature.
    /// </summary>
    public class Server : IDisposable
    {
        private static readonly ILog Logger = LogManager.GetLogger<Server>();
        private SiLA2Server _silaServer;

        public Server(int portNumber, NetworkInterface networkInterface, string configFile)
        {
            _silaServer = new SiLA2Server(new ServerInformation(
                    "Plate Reader",
                    "A SiLA Server made for training purposes",
                    "www.sila-standard.org",
                    "1.0"),
                portNumber,
                networkInterface,
                configFile);

            _silaServer.StartServer();
        }

        public void Dispose()
        {
            _silaServer.ShutdownServer();
        }
    }
}