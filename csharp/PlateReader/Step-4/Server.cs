using System;
using Common.Logging;
using System.Net.NetworkInformation;
using Sila2.Org.Silastandard.Training.Covercontroller.V1;
using Sila2.Org.Silastandard.Training.Temperatureprovider.V1;
using Sila2.Server;
using Sila2;
using Sila2.Org.Silastandard.Training.Shakecontroller.V1;

namespace SilaTraining.PlateReader
{
    /// <summary>
    /// Implements a SiLA 2 Server that provides a GreetingProvider and a TemperatureController feature.
    /// </summary>
    public class Server : IDisposable
    {
        private static readonly ILog Logger = LogManager.GetLogger<Server>();
        private SiLA2Server _silaServer;

        public Server(int portNumber, NetworkInterface networkInterface, string configFile)
        {
            _silaServer = new SiLA2Server(new ServerInformation(
                    "Plate Reader",
                    "A SiLA Server made for training purposes",
                    "www.sila-standard.org",
                    "1.0"),
                portNumber,
                networkInterface,
                configFile);

            _silaServer.ReadFeature("features/CoverController.sila.xml");
            _silaServer.GrpcServer.Services.Add(CoverController.BindService(new CoverControllerImpl()));
            _silaServer.ReadFeature("features/TemperatureProvider.sila.xml");
            _silaServer.GrpcServer.Services.Add(TemperatureProvider.BindService(new TemperatureProviderImpl()));
            _silaServer.ReadFeature("features/ShakeController.sila.xml");
            _silaServer.GrpcServer.Services.Add(ShakeController.BindService(new ShakeControllerImpl()));

            _silaServer.StartServer();
        }

        public void Dispose()
        {
            _silaServer.ShutdownServer();
        }
    }
}