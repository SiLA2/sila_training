package sila_training.java.multidrop;

import sila_java.library.core.encryption.SelfSignedCertificate;
import sila_java.library.server_base.SiLAServer;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.server_base.utils.ArgumentHelper;

import java.io.IOException;

import static sila_java.library.core.utils.FileUtils.getResourceContent;
import static sila_java.library.core.utils.Utils.blockUntilStop;

public class MultidropServer implements AutoCloseable {
    public static final String SERVER_TYPE = "Multidrop Micro";
    private final SiLAServer siLAServer;
    private final ShakeControllerImpl shakeController;

    public MultidropServer(final ArgumentHelper argumentHelper) {
        final ServerInformation serverInfo = new ServerInformation(
                SERVER_TYPE,
                "Liquid dispenser for 96 and 384 well plates with shaking function. Requires manual "
                        + "installation for a liquid reservoir",
                "http://www.titertek-berthold.com",
                "v0.0"
        );

        try {
            final SiLAServer.Builder builder;
            if (argumentHelper.getConfigFile().isPresent()) {
                builder = SiLAServer.Builder.withConfig(argumentHelper.getConfigFile().get(), serverInfo);
            } else {
                builder = SiLAServer.Builder.withoutConfig(serverInfo);
            }
            argumentHelper.getPort().ifPresent(builder::withPort);
            argumentHelper.getInterface().ifPresent(builder::withDiscovery);

            if (argumentHelper.useEncryption()) {
                builder.withSelfSignedCertificate();
            }

            shakeController = new ShakeControllerImpl();
            builder.addFeature(
                    getResourceContent("ShakeController.sila.xml"),
                    shakeController);

            this.siLAServer = builder.start();
        } catch (IOException | SelfSignedCertificate.CertificateGenerationException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        this.siLAServer.close();
    }

    public static void main(final String[] args) {
        final ArgumentHelper argumentHelper = new ArgumentHelper(args, SERVER_TYPE);
        try (final MultidropServer server = new MultidropServer(argumentHelper)) {
            Runtime.getRuntime().addShutdownHook(new Thread(server::close));
            blockUntilStop();
        }
    }
}