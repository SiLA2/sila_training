package sila_training.java.multidrop;

import io.grpc.stub.StreamObserver;
import sila2.ch.unitelabs.none.shakecontroller.v1.ShakeControllerGrpc;
import sila2.ch.unitelabs.none.shakecontroller.v1.ShakeControllerOuterClass;
import sila_java.library.core.sila.types.SiLAErrors;
import sila_java.servers.multidrop.handler.MultidropDriverReal;

import java.io.IOException;

public class ShakeControllerImpl extends ShakeControllerGrpc.ShakeControllerImplBase {
    @Override
    public void shake(ShakeControllerOuterClass.Shake_Parameters request, StreamObserver<ShakeControllerOuterClass.Shake_Responses> responseObserver) {

        MultidropDriverReal multidrop = new MultidropDriverReal();
        try {
            long shakeDuration = request.getDuration().getValue();
            multidrop.shake((int) shakeDuration);
            responseObserver.onNext(ShakeControllerOuterClass.Shake_Responses.newBuilder().build());
            responseObserver.onCompleted();
        } catch (IOException e) {
            responseObserver.onError(SiLAErrors.generateDefinedExecutionError("IOException", e.getMessage()));
        }
    }
}
