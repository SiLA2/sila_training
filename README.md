# SiLA 2 Training - Code Tutorials
This repository has been originally created in order to support the technical [SiLA 2 Training Course](https://sila-standard.com/training/).
It can also be used as a reference for understanding step-by-step how a SiLA server is implemented in each of the reference implementations. 


## Content

|Project | Description | Reference Language|
| ------ | ----------- | ------------------|
|[csharp/PlateReader](csharp/PlateReader) | A step-by-step implementation of a dummy/simulated PlateReader server | [sila_csharp](https://gitlab.com/SiLA2/sila_csharp) | 
|[java/multidrop](java/multidrop) | An example implementation of a Multidrop server using the multidrop library available in `sila_java` | [sila_java](https://gitlab.com/SiLA2/sila_java) | 

### Coming Soon

 - A step-by-step implementation of a server in Python

 - A step-by-step implementation of a server in Java

## Contact
If you have any questions regarding this repository feel free to contact the maintainer Ricardo Gaviria (<ricardo@unitelabs>) or reach out via [slack](https://join.slack.com/t/sila-standard/shared_invite/enQtNDI0ODcxMDg5NzkzLTBhOTU3N2I0NTc4NDcyMjg2ZDIwZDc1Yjg4N2FmYjZkMzljZDAyZjAwNTc5OTVjYjIwZWJjYjA0YTY0NTFiNDA)
